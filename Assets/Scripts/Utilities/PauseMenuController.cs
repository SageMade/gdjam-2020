﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System;

public class PauseMenuController : MonoBehaviour
{
    public Slider MusicSlider;
    public Slider SfxSlider;

    private void Awake() {
        MusicSlider?.SetValueWithoutNotify(PlayerPrefs.GetFloat("audio.music_volume", 0.0f));
        MusicSlider?.onValueChanged?.AddListener(OnMusicChanged);
        SfxSlider?.SetValueWithoutNotify(PlayerPrefs.GetFloat("audio.sfx_volume", 0.0f));
        SfxSlider?.onValueChanged?.AddListener(OnSfxChanged);
    }

    private void OnSfxChanged(float value)
    {
        AudioManager.Instance.SetSfxVolume(value);
        PlayerPrefs.Save();
    }

    private void OnMusicChanged(float value)
    {
        AudioManager.Instance.SetMusicVolume(value);
        PlayerPrefs.Save();
    }
}

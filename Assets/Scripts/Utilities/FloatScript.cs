﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatScript : MonoBehaviour
{
    public float MaxDrift = 0.25f;
    public float CycleTime = 2.0f;

    float _startY;

    // Start is called before the first frame update
    void Start()
    {
        _startY = transform.position.y;  
    }

    // Update is called once per frame
    void Update()
    {
        float offset = Mathf.Sin(2 * Mathf.PI * (Time.time / CycleTime)) * MaxDrift;
        Vector3 pos = transform.position;
        pos.y = _startY + offset;
        transform.position = pos;
    }
}

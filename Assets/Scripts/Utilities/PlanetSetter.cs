﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetSetter : MonoBehaviour
{
    public List<GameObject> planets;

    // Start is called before the first frame update
    void Awake()
    {
        foreach(Transform child in transform)
        {
            planets.Add(child.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangePlanetColours()
    {
        foreach(GameObject p in planets)
        {
            float r, g, b;
            r = Random.Range(0f, 1f);
            g = Random.Range(0f, 1f);
            b = Random.Range(0f, 1f);
            p.GetComponent<MeshRenderer>().material.color = new Color(r, g, b, 1f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions
{
    /// <summary>
    /// Destroys all children of this game object
    /// </summary>
    /// <param name="instance"></param>
    /// <returns></returns>
    public static GameObject BurnTheOrphans(this GameObject instance)
    {
        foreach(Transform t in instance.transform) {
            GameObject.Destroy(t.gameObject);
        }
        return instance;
    }

    public static T GetOrAssign<T>(this GameObject instance) where T : UnityEngine.Component {
        T result = instance.GetComponent<T>();
        if (result == null)
            result = instance.AddComponent<T>();
        return result;
    }
    public static T GetInChildrenOrAssign<T>(this GameObject instance) where T : UnityEngine.Component
    {
        T result = instance.GetComponentInChildren<T>();
        if (result == null)
            result = instance.AddComponent<T>();
        return result;
    }
    public static T GetInChildrenOrAssign<T>(this GameObject instance, bool includeInactive) where T : UnityEngine.Component
    {
        T result = instance.GetComponentInChildren<T>(includeInactive);
        if (result == null)
            result = instance.AddComponent<T>();
        return result;
    }
}

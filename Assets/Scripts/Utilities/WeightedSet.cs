﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using UnityEngine;

public interface IWeighted<T> {
    T Value { get; set; }
    float Weight { get; set; }
}

/// <summary>
/// Stores a list of items with associated rates. Allows us to randomly select items
/// from a weighted list
/// </summary>
/// <typeparam name="T">The underlying value type to store</typeparam>
[System.Serializable]
public class WeightedSet<T> : IEnumerable<T>, ISerializable {
    /// <summary>
    /// Represents a callback function for determining the weight of an item
    /// </summary>
    /// <param name="value">The value to determine the weight for</param>
    /// <returns>The weight of the item</returns>
    public delegate float WeightInitializer(T value);

    /// <summary>
    /// Represents an entry in our list (stores the Value and the Weight)
    /// </summary>
    [Serializable]
    public class Entry : ISerializable, IWeighted<T> {
        [SerializeField]
        public T Value;
        [SerializeField]
        public float Weight;

        public Entry(T value, float weight)
        {
            Value = value;
            Weight = weight;
        }

        protected Entry(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");

            Value = (T)info.GetValue("Value", typeof(T));
            Weight = info.GetSingle("Weight");
        }

        T IWeighted<T>.Value { get => Value; set => Value = value; }
        float IWeighted<T>.Weight { get => Weight; set => Weight = value; }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Value", Value);
            info.AddValue("Weight", Weight);
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");

            GetObjectData(info, context);
        }
    }

    /// <summary>
    /// Gets a value with the given index
    /// </summary>
    /// <param name="index">The index of the value to get</param>
    /// <returns>The value at index</returns>
    public T this[int index] {
        get { return myEntries[index].Value; }
        set { myEntries[index].Value = value; }
    }
    /// <summary>
    /// Gets the value at the given point in the distribution. This will stay the same
    /// for a given value until the collection is modified. The value is given in the range
    /// 0-1
    /// </summary>
    /// <param name="val">The value to look up (between 0 and 1)</param>
    /// <returns>The value at the given point in the distribution</returns>
    public T this[float val] {
        get { return Select(val); }
    }

    /// <summary>
    /// Gets the number of elements in this collection
    /// </summary>
    public int Count {
        get { return myEntries.Count; }
    }

    /// <summary>
    /// Gets a read-only enumerable list of weighted entries
    /// </summary>
    public IEnumerable<Entry> Entries {
        get { return myEntries.Skip(0); }
    }

    [SerializeField]
    private List<Entry> myEntries;
    [SerializeField, HideInInspector]
    private float myTotalWeight;

    /// <summary>
    /// Creates a new empty weighted set
    /// </summary>
    public WeightedSet()
    {
        myEntries = new List<Entry>();
        myTotalWeight = 0.0f;
    }

    /// <summary>
    /// Creates a new weighted set from a list, with all elements having the same weight. This is the
    /// same as creating an empty set and calling AddRange
    /// </summary>
    /// <param name="list">The items to initialize the list with</param>
    /// <param name="value">The weight per item in the list</param>
    public WeightedSet(IEnumerable<T> list, float value = 1) : this()
    {
        AddRange(list, value);
    }

    /// <summary>
    /// Creates a new weighted set from a given list, but uses a function to determine each 
    /// item's weight
    /// </summary>
    /// <param name="list">The list to initialize the set with</param>
    /// <param name="initializer">The function to determine an item's weight</param>
    public WeightedSet(IEnumerable<T> list, WeightInitializer initializer) : this()
    {
        foreach (T item in list)
        {
            myEntries.Add(new Entry(item, initializer?.Invoke(item) ?? 0.0f));
        }
    }

    /// <summary>
    /// Serialization constructor
    /// </summary>
    /// <param name="info">The serialized information</param>
    /// <param name="context">The context in which serialization is occuring</param>
    protected WeightedSet(SerializationInfo info, StreamingContext context)
    {
        if (info == null)
            throw new ArgumentNullException("info");

        myEntries = (List<Entry>)info.GetValue("Entries", typeof(List<Entry>));
        myTotalWeight = info.GetSingle("TotalWeight");
    }

    /// <summary>
    /// Adds a new item to the set with a given weight
    /// </summary>
    /// <param name="value">The item to add</param>
    /// <param name="weight">The weight of the item</param>
    public void Add(T value, float weight)
    {
        if (weight < 0.0f)
            throw new ArgumentException("Weight cannot be negative");
        else if (weight == 0.0f)
            Debug.LogWarning("Adding an item with zero weight, this will not be selected. Is this in error?");

        myEntries.Add(new Entry(value, weight));
        myTotalWeight += weight;
    }

    /// <summary>
    /// Adds a new weighted item to the set
    /// </summary>
    /// <param name="value">The item to add</param>
    public void Add(IWeighted<T> value)
    {
        Add(value.Value, value.Weight);
    }

    /// <summary>
    /// Adds a range of items to the set, all using the given weight
    /// </summary>
    /// <param name="range">The list of items to add</param>
    /// <param name="val">The weight for the individual items</param>
    public void AddRange(IEnumerable<T> range, float val = 1.0f)
    {
        foreach (T item in range)
            Add(item, val);
    }

    /// <summary>
    /// Adds a range of weighted items to the set
    /// </summary>
    /// <param name="range">The list of items to add</param>
    public void AddRange<V>(IEnumerable<V> range) where V : IWeighted<T>
    {
        foreach (IWeighted<T> item in range)
            Add(item.Value, item.Weight);
    }

    /// <summary>
    /// Determines the percentage change of an item with the given weight being picked from the set
    /// </summary>
    /// <param name="weight">The weight to test</param>
    /// <param name="existing">True to only use existing weights, false if we want to include the item's weight in the calculation</param>
    /// <returns>The percentage chance of picking an item with the given weight</returns>
    public float GetPercentage(float weight, bool existing = true)
    {
        if (existing)
            return myTotalWeight > 0 ? weight / myTotalWeight : 0;
        else
            return weight + myTotalWeight > 0 ? weight / (weight + myTotalWeight) : 0;
    }

    /// <summary>
    /// Removes all items from this set
    /// </summary>
    public void Clear()
    {
        myEntries.Clear();
    }

    /// <summary>
    /// Selects a random item from the weighted set
    /// </summary>
    /// <returns>A random item from the set</returns>
    public T Rand()
    {
        return Select(UnityEngine.Random.value);
    }

    /// <summary>
    /// Selects an item from the set with a given random value between 0 and 1
    /// </summary>
    /// <param name="rand">The random value to lookup</param>
    /// <returns>A random item from the set</returns>
    public T Select(float rand)
    {
        rand = Mathf.Clamp(rand, 0, 1.0f);
        float tVal = rand * myTotalWeight;
        T result = myEntries[myEntries.Count - 1].Value;
        for (int ix = 0; ix < myEntries.Count; ix++)
        {
            if (tVal < myEntries[ix].Weight)
            {
                result = myEntries[ix].Value;
                break;
            }
            tVal -= myEntries[ix].Weight;
        }
        return result;
    }

    public IEnumerator<T> GetEnumerator()
    {
        foreach (Entry e in myEntries)
        {
            yield return e.Value;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    /// <summary>
    /// Serialization writing method
    /// </summary>
    /// <param name="info">The serialized information</param>
    /// <param name="context">The context in which serialization is occuring</param>
    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("Entries", myEntries);
        info.AddValue("TotalWeight", myTotalWeight);
    }

    /// <summary>
    /// Serialization writing method
    /// </summary>
    /// <param name="info">The serialized information</param>
    /// <param name="context">The context in which serialization is occuring</param>
    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
    void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
    {
        if (info == null)
            throw new ArgumentNullException("info");

        GetObjectData(info, context);
    }
}

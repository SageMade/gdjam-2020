﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour
{
    public Vector2 mouseInput;

    private Vector3 defaultRotation;

    private void Awake()
    {
        defaultRotation = transform.eulerAngles;
    }

    private void OnEnable()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void OnDisable()
    {
        Cursor.lockState = CursorLockMode.None;
    }

    // Update is called once per frame
    void Update()
    {
        rotateCamera();
    }

    void rotateCamera()
    {
        mouseInput.x -= Input.GetAxis("Mouse Y");
        mouseInput.y += Input.GetAxis("Mouse X");

        mouseInput.x = Mathf.Clamp(mouseInput.x, -89f, 89f);

        transform.eulerAngles = defaultRotation + new Vector3(mouseInput.x, mouseInput.y, 0f);
    }

    private void OnDestroy()
    {
        Cursor.lockState = CursorLockMode.None;
    }
}

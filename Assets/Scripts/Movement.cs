﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Camera mainCam;
    Rigidbody rb;

    [Range(1f, 5f)]
    public float maxSpeed = 2f;

    // Start is called before the first frame update
    void Awake()
    {
        mainCam = Camera.main;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();

        ClampVelocity();
    }

    void Move()
    {
        Vector2 moveInput;
        moveInput.x = Input.GetAxis("Horizontal");
        moveInput.y = Input.GetAxis("Vertical");

        Vector3 tempForward = mainCam.transform.forward;

        mainCam.transform.forward = Vector3.Normalize(new Vector3(tempForward.x, 0f, tempForward.z));

        Vector3 moveDir = mainCam.transform.TransformVector(new Vector3(moveInput.x, 0f, moveInput.y));

        mainCam.transform.forward = tempForward;

        rb.AddForce(moveDir, ForceMode.VelocityChange);
    }

    void ClampVelocity()
    {
        rb.velocity = VecClamp(rb.velocity, maxSpeed);
    }

    Vector3 VecClamp(Vector3 vec, float maxMagnitude)
    {
        if(vec.magnitude < maxMagnitude)
        {
            return vec;
        }

        vec /= vec.magnitude / maxSpeed;

        return vec;
    }
}

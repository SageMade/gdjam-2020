﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour 
{
    public float MusicVolumeOffset = -20.0f;
    public float SfxVolumeOffset = -10.0f;

    public AudioMixerGroup MusicGroup;
    public AudioMixerGroup SfxGroup;

    public AudioSource CharacterSource;
    public AudioSource MusicSource;
    public AudioSource SfxSource;

    public AudioClip DuplicateItemSound;
    public AudioClip[] StrikeSounds;
    public AudioClip[] WarpInSounds;
    public AudioClip[] WarpOutSounds;

    public AudioClip[] MusicClips;

    public static AudioManager Instance {
        get; private set;
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
            return;
        }
    }

    public void SetSfxVolume(float value)
    {
        PlayerPrefs.SetFloat("audio.sfx_volume", value);
        SfxGroup.audioMixer.SetFloat(SfxGroup.name + "_Volume", value + SfxVolumeOffset);
    }

    public void SetMusicVolume(float value)
    {
        PlayerPrefs.SetFloat("audio.music_volume", value);
        MusicGroup.audioMixer.SetFloat(MusicGroup.name + "_Volume", value + MusicVolumeOffset);
    }

    // Start is called before the first frame update
    void Start()
    {
        SetMusicVolume(PlayerPrefs.GetFloat("audio.music_volume", 0.0f));
        SetSfxVolume(PlayerPrefs.GetFloat("audio.sfx_volume", 0.0f));

        if (MusicClips.Length > 0) {
            StartCoroutine(WaitForEndOfSong());
        }
    }

    IEnumerator WaitForEndOfSong() {
        while (enabled) {
            MusicSource.clip = MusicClips.TakeRandom();
            MusicSource.Play();
            yield return new WaitForSecondsRealtime(MusicSource.clip.length);
        }
    }

    public void PlayDuplicateSelectSfx()
    {
        SfxSource?.PlayOneShot(DuplicateItemSound);
    }

    public void PlayStrikeSound()
    {
        SfxSource?.PlayOneShot(StrikeSounds?.TakeRandom());
    }

    public void PlayWarpInSound()
    {
        SfxSource?.PlayOneShot(WarpInSounds?.TakeRandom());
    }
    public void PlayWarpOutSound()
    {
        SfxSource?.PlayOneShot(WarpOutSounds?.TakeRandom());
    }

    // Update is called once per frame
    public void PlayCharacterVoiceLine(AudioClip clip) {
        if (clip != null) {
            CharacterSource.PlayOneShot(clip);
        }
    }
}

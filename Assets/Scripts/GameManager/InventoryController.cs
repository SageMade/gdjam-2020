﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour
{
    public const float NULL_PERCENTAGE_CHANCE = 0.1f;

    [SerializeField]
    GameObject[] ItemFrames;

    private GameObject _workingInstance;
    public Sprite DefaultSprite;
    
    public void Awake()
    {
        ItemFrames = ItemFrames.Where(x => x != null).ToArray();
        for (int ix = 0; ix < ItemFrames.Length; ix++)
        {
            ItemClick click = ItemFrames[ix].GetInChildrenOrAssign<ItemClick>();
            click.ItemEvent = click.ItemEvent ?? new UnityEngine.Events.UnityEvent<GameObject>();
            click.ItemEvent.AddListener(OnItemClicked);
            _PopulateItem(click.gameObject);
        }
    }

    public void OnItemClicked(GameObject instance) {
        if (GameManager.Instance.CurrentSelectedItem == null) {
            InventoryItemInstance item = instance.GetComponent<InventoryItemInstance>();
            GameManager.Instance.SetSelectedItem(item.Item);
            instance.GetComponentInChildren<Image>().enabled = false;
            _workingInstance = instance;
        } else if (instance == _workingInstance) {
            GameManager.Instance.SetSelectedItem(null);
            _workingInstance.GetComponentInChildren<Image>().enabled = true;
            _workingInstance = null;
        }
        else {
            // TODO: play negative sound effect
            AudioManager.Instance.PlayDuplicateSelectSfx();
            Debug.LogWarning("Attempting to select a duplicate item");
        }
    }

    private void _PopulateItem(GameObject gameObject)
    {
        InventoryItemInstance item = gameObject.GetInChildrenOrAssign<InventoryItemInstance>();
        item.Item = ItemProvider.GenerateItem();
        Image image = gameObject.GetComponentInChildren<Image>();
        image.enabled = true;
        image.sprite = item.Item.Item.Icon == null ? DefaultSprite : item.Item.Item.Icon;
        image.color = item.Item.Tint?.Color ?? Color.white;
    }

    public void RepopulateWorkingItem()
    {
        if (_workingInstance != null) {
            _PopulateItem(_workingInstance);
        }
    }

    internal void ReturnWorkingItem()
    {
        if (_workingInstance != null) {
            GameManager.Instance.SetSelectedItem(null);
            _workingInstance.GetComponentInChildren<Image>().enabled = true;
            _workingInstance = null;
        }
    }


    public ItemInstance GetItemOrNull()
    {
        bool hasItem = Random.Range(0.0f, 1.0f) >= NULL_PERCENTAGE_CHANCE;
        if (!hasItem) {
            Debug.Log("Throwing a curveball!");
            return null;
        } else {
            return ItemFrames.TakeRandom().GetComponentInChildren<InventoryItemInstance>().Item;
        }
    }

    public ItemInstance TryFindItem(ItemInstance requestedItem)
    {
        return ItemFrames.Select(x => x.GetComponentInChildren<InventoryItemInstance>().Item).FirstOrDefault(x => x.Equals(requestedItem));
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientInteractionController : MonoBehaviour
{
    private DialogueController _dialogue;
    private GameObject _characterInstance;

    public MeshRenderer MissingMesh;

    public GameObject WarpOutEffect;
    public GameObject WarpInEffect;
    public Material DissolveShader;

    public CharacterInstance Character {
        get; private set;
    }

    private void Awake() {
        _dialogue = FindObjectOfType<DialogueController>(true);
    }

    public void Interact(GameObject host)
    {
        if (Character != null) {
            _dialogue.Interact(Character);
        }
    }

    public void SetCharacter(CharacterInstance character)
    {
        Character = character;
        if (_characterInstance != null) { Destroy(_characterInstance); }
        if (character.Template.Prefab != null) {
            MissingMesh.enabled = false;
            _characterInstance = Instantiate(character.Template.Prefab, transform);
        } else {
            MissingMesh.enabled = true;
        }
        // Todo: play teleport sfx
    }

    public IEnumerator DissolveCharacter(float time)
    {
        if (_characterInstance != null) {
            Destroy(_characterInstance);
            AudioManager.Instance.PlayWarpOutSound();
            GameObject warpOut = Instantiate(WarpOutEffect, transform);
            yield return new WaitForSeconds(time);
            Destroy(warpOut);
            yield break;
        } else {
            yield break;
        }
        /*
        float alpha = 1.0f;
        float delta = 1.0f / time;
        while (alpha > 0.0f)
        {
            float dt = Time.deltaTime;
            alpha -= dt * delta;
            DissolveShader.SetFloat("DissolveState", alpha);
            yield return new WaitForEndOfFrame();
        }
        yield break;
        */
    }

    public IEnumerator WarpInCharacter(CharacterInstance currentCharacter, float time)
    {
        SetCharacter(currentCharacter);
        AudioManager.Instance.PlayWarpInSound();
        GameObject warpIn = Instantiate(WarpInEffect, transform);
        yield return new WaitForSeconds(time);
        Destroy(warpIn);
        yield break;
        /*
        MeshRenderer[] renderers = _characterInstance.GetComponentsInChildren<MeshRenderer>();
        Material[] cachedMats = new Material[renderers.Length];
        for(int ix = 0; ix <renderers.Length; ix++) {
            cachedMats[ix] = renderers[ix].material;
            renderers[ix].material = DissolveShader;
            renderers[ix].material.SetTexture("Texture", cachedMats[ix].GetTexture("_BaseMap"));
        }
        float alpha = 0.0f;
        float delta = 1.0f / time;
        while (alpha < 1.0f)
        {
            float dt = Time.deltaTime;
            alpha += dt * delta;
            for (int ix = 0; ix < renderers.Length; ix++) {
                renderers[ix].material.SetFloat("DissolveState", alpha);
            }
            yield return new WaitForEndOfFrame();
        }
        for (int ix = 0; ix < renderers.Length; ix++)
        {
            renderers[ix].material = cachedMats[ix];
        }
        yield break;
        */
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueController : MonoBehaviour
{
    public GameObject RootCanvas;
    public TMPro.TextMeshProUGUI NameText;
    public TMPro.TextMeshProUGUI DialogueText;
    public GameObject OptionsCanvas;

    public enum State {
        Waiting, 
        Welcome,
        RespondingToClient,
        ClientResponding,
        ClientLeaving,
        Hidden
    }

    private State _state;
    private State _prevState;

    private bool _skip;

    public bool IsVisible {
        get { return RootCanvas?.activeInHierarchy ?? false; }
        set { RootCanvas?.SetActive((_state != State.Hidden && _state != State.Waiting) ? value : false); }
    }

    public void Interact(CharacterInstance character)
    {
        if (!enabled) return;

        switch (_state)
        {
            case State.Waiting:
                WelcomeCharacter(character);
                break;
            case State.Hidden:
                _state = State.RespondingToClient;
                break;
        }
    }

    public void LoadDialogue(string charName, string text)
    {
        NameText?.SetText(charName ?? "<missing>");
        DialogueText?.SetText(text ?? "<missing>");
    }

    void Start()
    {
        Reset();
    }

    public void Reset()
    {
        _state = State.Waiting;
        _prevState = _state;
        IsVisible = false;
        _skip = false;
        OptionsCanvas?.SetActive(false);
    }

    public void WelcomeCharacter(CharacterInstance character) {
        NameText?.SetText(GameManager.Instance.CurrentCharacter?.Name ?? "<missing>");
        LoadDialogue(character?.Name, DialogueGenerator.GetWelcomeDialogueText(character, GameManager.Instance.RequestedItem));
        _state = State.Welcome;
        IsVisible = true;
        AudioManager.Instance.PlayCharacterVoiceLine(character.Template.WelcomeSound);
        // TODO: Play welcome sound
    }

    private bool CheckKeyboardKeys()
    {
        if (_skip) { return true; }
        if (Input.anyKeyDown)
        {
            if (Input.GetMouseButtonDown(0)
                || Input.GetMouseButtonDown(1)
                || Input.GetMouseButtonDown(2))
                return false;
            return true;
        }
        return false;
    }

    public void Update()
    {
        switch(_state) {
            case State.Welcome:
                if (CheckKeyboardKeys()) {
                    _state = State.Hidden;
                }
                break;
            case State.ClientResponding:
                if (CheckKeyboardKeys()) {
                    _state = State.RespondingToClient;
                }
                break;
            case State.RespondingToClient:
                if (Input.GetKeyDown(KeyCode.Alpha1)) { AskForDescription(); }
                if (Input.GetKeyDown(KeyCode.Alpha2)) { GiveItem(); }
                if (Input.GetKeyDown(KeyCode.Alpha3)) { TellNoItem(); }
                if (Input.GetKeyDown(KeyCode.Alpha4) || _skip) { _state = State.Hidden; }
                break;
            case State.ClientLeaving:
                if (CheckKeyboardKeys()) {
                    _state = State.Waiting;
                    GameManager.Instance.MoveNextCharacter();
                }
                break;
        }
        if (_state != _prevState) {
            OptionsCanvas?.SetActive(_state == State.RespondingToClient);
            DialogueText?.transform?.parent?.gameObject?.SetActive(_state != State.RespondingToClient);
            IsVisible = _state != State.Hidden && _state != State.Waiting;
            GameManager.Instance.SetMouseLookEnabled(!IsVisible);
            GameManager.Instance.SetGameUIVisible(!IsVisible);
            _skip = false;
        }
        _prevState = _state;
    }


    public void Advance()
    {
         _skip = true;
    }

    public void AskForDescription()
    {
        if (_state == State.RespondingToClient) {
            _state = State.ClientResponding;
            CharacterInstance character = GameManager.Instance.CurrentCharacter;
            LoadDialogue(character?.Name, DialogueGenerator.GetDescriptionText(character, GameManager.Instance.RequestedItem));
            // TODO: Play welcome sound
            AudioManager.Instance.PlayCharacterVoiceLine(character.Template.WelcomeSound);
        }
    }

    public void GiveItem()
    {
        if (_state == State.RespondingToClient)
        {
            if (GameManager.Instance.CurrentSelectedItem == null)
            {
                _state = State.ClientResponding;
                CharacterInstance character = GameManager.Instance.CurrentCharacter;
                LoadDialogue(character?.Name, DialogueGenerator.GetLyingText(character, GameManager.Instance.RequestedItem));
                AudioManager.Instance.PlayCharacterVoiceLine(character.Template.NegativeSound);
            }
            else
            {
                _state = State.ClientLeaving;
                bool correct = GameManager.Instance.SubmitItem();
                CharacterInstance character = GameManager.Instance.CurrentCharacter;
                if (correct) {
                    LoadDialogue(character?.Name, DialogueGenerator.GetRightItemText(character, GameManager.Instance.RequestedItem));
                    AudioManager.Instance.PlayCharacterVoiceLine(character.Template.PositiveSound);
                }
                else
                {
                    LoadDialogue(character?.Name, DialogueGenerator.GetWrongItemText(character, GameManager.Instance.RequestedItem));
                    AudioManager.Instance.PlayCharacterVoiceLine(character.Template.NegativeSound);
                }
            }
        }
    }

    public void TellNoItem()
    {
        if (_state == State.RespondingToClient) {
            _state = State.ClientLeaving;
            bool correct = GameManager.Instance.SubmitNothing();
            CharacterInstance character = GameManager.Instance.CurrentCharacter;
            LoadDialogue(character?.Name, DialogueGenerator.GetNoItemText(character, GameManager.Instance.RequestedItem));
            AudioManager.Instance.PlayCharacterVoiceLine(character.Template.SadSound);
        }
    }
}

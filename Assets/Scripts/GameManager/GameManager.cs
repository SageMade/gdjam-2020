﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Material SkyboxMat;
    public PlanetSetter planets;

    public const int MAX_STRIKES = 3;

    private int _Correct;
    private int _Incorrect;

    public int Correct { get { return _Correct; } }
    public int Incorrect { get { return _Incorrect; } }

    private ItemInstance[] _inventory;
    private ItemInstance _currentTarget;
    private CharacterInstance _currentCharacter;

    public GameObject Player;
    public GameObject GameplayUIRoot;
    public GameObject PauseMenu;
    public GameObject GameOverMenu;
    public UnityEngine.UI.Image PlayerInventoryImage;
    public TMPro.TextMeshProUGUI ScoreLabel;

    public Camera StartCam;

    public AudioClip[] LossSounds;
    public AudioSource GameSoundSource;

    public static GameManager Instance {
        get; private set;
    }

    public CharacterInstance CurrentCharacter {
        get { return _currentCharacter; }
    }
    public ItemInstance CurrentTarget {
        get { return _currentTarget; }
    }
    public ItemInstance RequestedItem {
        get; protected set;
    }

    [System.NonSerialized]
    public ItemInstance CurrentSelectedItem;

    public HorizontalLayoutGroup StrikeContainer;
    public GameObject StrikePrefab;

    public InventoryController Inventory;
    public DialogueController DialogueController;
    public ClientInteractionController ClientController;

    private void Awake() {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
            return;
        }

        DialogueController = DialogueController ?? FindObjectOfType<DialogueController>(true);
        Inventory = Inventory ?? FindObjectOfType<InventoryController>(true);
        ClientController = ClientController ?? FindObjectOfType<ClientInteractionController>(true);
    }

    internal void SetSelectedItem(ItemInstance item)
    {
        CurrentSelectedItem = item;
        PlayerInventoryImage.enabled = item != null;
        if (item != null) {
            PlayerInventoryImage.sprite = item.Item.Icon;
            PlayerInventoryImage.color = item?.Tint?.Color ?? Color.white;
        }
    }

    // Start is called before the first frame update
    void Start() {

        _inventory = new ItemInstance[16];
        for(int ix = 0; ix < _inventory.Length; ix++) {
            _inventory[ix] = ItemProvider.GenerateItem();
        }
    }

    public void StartGame() {
        _Incorrect = 0;
        _Correct = 0;
        StartCam.gameObject.SetActive(false);
        GameOverMenu?.SetActive(false);
        PauseMenu?.SetActive(false);
        DialogueController.Reset();
        Player.SetActive(true);
        PlayerInventoryImage.sprite = null;
        ClientController.enabled = true;
        DialogueController.enabled = true;
        ScoreLabel?.SetText("0 served");
        StrikeContainer.gameObject.BurnTheOrphans();
        SetMouseLookEnabled(true);
        SetGameUIVisible(true);
        MoveNextCharacter();
    }

    public void QuitGame() {
        #if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
        #else
         Application.Quit();
        #endif
    }

    private void _ChangeScore(bool correct)
    {
        if (correct) {
            _Correct++;
            ScoreLabel?.SetText(string.Format("{0} served", _Correct));
        }
        else {
            _Incorrect++;
            Instantiate(StrikePrefab, StrikeContainer.transform);
            StrikeContainer.CalculateLayoutInputHorizontal();
            AudioManager.Instance.PlayStrikeSound();
        }

        if(((_Correct + _Incorrect) % 2) == 0)
        {
            planets.ChangePlanetColours();
            float r, g, b;
            r = UnityEngine.Random.Range(0.25f, 0.5f);
            g = UnityEngine.Random.Range(0.25f, 0.5f);
            b = UnityEngine.Random.Range(0.25f, 0.5f);
            SkyboxMat.SetColor("Color_C54A6174", new Color(r, g, b, 1f));
        }
    }

    public void SetGameUIVisible(bool enabled) {
        GameplayUIRoot?.SetActive(enabled && _Incorrect < MAX_STRIKES);
    }

    public void ShowPauseMenu()
    {
        Time.timeScale = 0.0f;
        DialogueController.IsVisible = false;
        DialogueController.enabled = false;
        SetGameUIVisible(false);
        PauseMenu.SetActive(true);
        SetMouseLookEnabled(false);
    }

    public void HidePauseMenu()
    {
        Time.timeScale = 1.0f;
        DialogueController.IsVisible = true;
        DialogueController.enabled = true;
        SetGameUIVisible(false);
        PauseMenu.SetActive(false);
        SetMouseLookEnabled(true);
    }

    public void SetMouseLookEnabled(bool enabled)
    {
        Player.GetComponentInChildren<CamControl>().enabled = enabled && _Incorrect < MAX_STRIKES;
        Player.GetComponent<Movement>().enabled = enabled && _Incorrect < MAX_STRIKES;
    }

    public void Update()
    {
        KeyCode pauseKey = KeyCode.Escape;

        if (Input.GetKeyDown(pauseKey) && !StartCam.isActiveAndEnabled) {
            if (!PauseMenu.activeInHierarchy) { 
                ShowPauseMenu();
            } else {
                HidePauseMenu();
            }
        }
    }

    /// <summary>
    /// Attempt to give an item to the client, returns if it was the correct item or not
    /// </summary>
    /// <returns></returns>
    public bool SubmitItem() {
        bool result = false;
        if (_currentTarget == null) {
            result = CurrentSelectedItem == null;
        } else {
            result = _currentTarget.Equals(CurrentSelectedItem);
        }
        
        _ChangeScore(result);
        if (result) {
            SetSelectedItem(null);
            Inventory.RepopulateWorkingItem();
        } else {
            Inventory.ReturnWorkingItem();
        }
        return result;
    }

    public bool SubmitNothing() {
        bool result = _currentTarget == null;
        _ChangeScore(result);
        Inventory.ReturnWorkingItem();
        return result;
    }

    public void MoveNextCharacter() {
        if (_Incorrect >= MAX_STRIKES)
        {
            // TODO: game over
            Debug.LogWarning("Game over man!");
            StartCoroutine(InformOfLoss());
        }
        else
        {
            _currentTarget = Inventory.GetItemOrNull();
            RequestedItem = _currentTarget ?? ItemProvider.GenerateItem();
            if (_currentTarget == null)
            {
                Debug.Log("[GameMgr] Expected null item, scanning inventory to be safe");
                _currentTarget = Inventory.TryFindItem(RequestedItem);
            }
            StartCoroutine(SwapCharacters());
        }
    }

    private IEnumerator SwapCharacters()
    {
        yield return ClientController.DissolveCharacter(2.0f);
        _currentCharacter = CharacterProvider.GenerateCharacter();
        yield return ClientController.WarpInCharacter(_currentCharacter, 2.0f);

        Debug.LogFormat("[GameMgr] Target: {0}", _currentTarget);
        Debug.LogFormat("[GameMgr] Requested: {0}", RequestedItem);

        yield break;
    }

    private IEnumerator InformOfLoss()
    {
        ClientController.enabled = false;
        DialogueController.IsVisible = false;
        DialogueController.enabled = false;
        SetMouseLookEnabled(false);
        Cursor.lockState = CursorLockMode.Locked;
        AudioClip clip = LossSounds.TakeRandom();
        if (clip != null) {
            GameSoundSource.PlayOneShot(clip);
        }
        yield return new WaitForSecondsRealtime(clip?.length ?? 0.0f);
        GameOverMenu?.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }
}

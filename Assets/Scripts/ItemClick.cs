﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ItemClick : MonoBehaviour
{
    MeshRenderer mRenderer;

    [SerializeField]
    public UnityEvent<GameObject> ItemEvent;

    [Range(1f, 3f)]
    public float intensity = 1f;

    // Start is called before the first frame update
    void Awake()
    {
        mRenderer = GetComponent<MeshRenderer>();
    }

    private void OnMouseDown()
    {
        //Debug.Log("Object Clicked");
        ItemEvent?.Invoke(gameObject);
    }

    private void OnMouseEnter()
    {
        //Debug.Log("Object Hovered");
        mRenderer.material.SetFloat("FresnelControl", intensity);
    }

    private void OnMouseExit()
    {
        //Debug.Log("Object Not Hovered Anymore");
        mRenderer.material.SetFloat("FresnelControl", 0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

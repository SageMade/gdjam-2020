﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ItemProvider
{
    private static ItemDescription[] _descriptions;
    private static WeightedSet<ItemDescription> _weighted;

    static ItemProvider()
    {
        Resources.LoadAll<ColorTint>("Colors");
        Resources.LoadAll<ItemDescription>("Items");
        _descriptions = Resources.FindObjectsOfTypeAll<ItemDescription>();
        _weighted = new WeightedSet<ItemDescription>();
        foreach (ItemDescription description in _descriptions)
        {
            _weighted.Add(description, ((description.TintOptions?.Length ?? 0) + 1) * description.WeightMultiplier);
        }
    }

    public static ItemDescription SelectItem() {
        return _weighted.Rand();
    }

    public static ItemInstance GenerateItem() {
        ItemDescription description = SelectItem();
        ItemInstance instance = new ItemInstance();
        instance.Item = description;
        instance.Tint = description.TintOptions?.TakeRandom();
        instance.Prefix = description.PrefixWords?.TakeRandom();
        Debug.LogFormat(
            "[ItemGen] Create instance of {0} (tint: {1}, prefix: {2}) - {3}", 
            description.Name, 
            instance.Tint?.name ?? "null", 
            instance.Prefix ?? "null", 
            instance.ToString()
        );
        return instance;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Items/Color Tint", fileName = "color")]
public class ColorTint : ScriptableObject, IEquatable<ColorTint> {
    public Color Color;
    public string ReadableName;

    public override bool Equals(object other) {
        if (other == null)
            return false;

        if (!(other is ColorTint))
            return false;

        ColorTint instance = other as ColorTint;
        return Equals(instance);
    }

    public bool Equals(ColorTint other) {
        return other != null &&
               Color.Equals(other.Color) &&
               ReadableName == other.ReadableName;
    }

    public override int GetHashCode() {
        var hashCode = -412754060;
        hashCode = hashCode * -1521134295 + base.GetHashCode();
        hashCode = hashCode * -1521134295 + EqualityComparer<Color>.Default.GetHashCode(Color);
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ReadableName);
        return hashCode;
    }
}

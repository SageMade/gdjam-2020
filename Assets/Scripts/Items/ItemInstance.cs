﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemInstance : IEquatable<ItemInstance> {
    static readonly Color PREFIX_COLOR = new Color(255 / 255.0f, 165 / 255.0f, 0.0f);
    public ItemDescription Item;
    public ColorTint       Tint;
    public string          Prefix;

    public override bool Equals(object obj)
    {
        if (obj is null)
            return false;

        if (!(obj is ItemInstance))
            return false;

        ItemInstance other = obj as ItemInstance;
        return Equals(other);
    }

    public bool Equals(ItemInstance other)
    {
        return other != null &&
               Item == other.Item &&
               (Tint?.Equals(other.Tint) ?? other.Tint == null);
    }

    public override int GetHashCode() {
        var hashCode = -1609654309;
        hashCode = hashCode * -1521134295 + EqualityComparer<ItemDescription>.Default.GetHashCode(Item);
        hashCode = hashCode * -1521134295 + EqualityComparer<ColorTint>.Default.GetHashCode(Tint);
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Prefix);
        return hashCode;
    }

    public override string ToString()
    {
        string result = "";
        if (!string.IsNullOrWhiteSpace(Prefix)) {
            result += string.Format("{1} ", ColorUtility.ToHtmlStringRGBA(PREFIX_COLOR), Prefix);
        }
        if (Tint != null) {
            result += string.Format("<i><color=#{0}>{1}</color></i> ", ColorUtility.ToHtmlStringRGBA(Tint.Color), Tint.ReadableName);
        }
        result += string.Format("<b><i>{0}</i></b>", Item.Name);
        return result;
    }
}

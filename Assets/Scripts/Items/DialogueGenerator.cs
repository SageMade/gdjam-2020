﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class DialogueGenerator
{
    private static bool IsVowel(this char c)
    {
        return "aeiou".IndexOf(c.ToString(), StringComparison.InvariantCultureIgnoreCase) >= 0;
    }

    private static string DiagGen(this string search, string replace, string with) {
        search = search.Trim();
        if (string.IsNullOrWhiteSpace(with))
        {
            return search.Replace(replace, "");
        }
        else
        {
            with = with == null ? "<missing>" : with;
            with = with.Trim().Replace("  ", " ");
            if (search.EndsWith(".") || search.Length == 0)
            {
                with = char.ToUpper(with[0]) + with.Substring(1);
            }
            search += " ";
            return search.Replace(replace, with);
        }
    }

    private static string ReplaceText(string text, ItemInstance item, string charName) {
        text = string.IsNullOrWhiteSpace(text) ? null : text;
        string itemShortDesc = item.ToString();
        ItemDescription desc = item.Item;
        // Determine the article based on item short description (so that if tints start with a vowel it still makes sense)
        Article article = 
            itemShortDesc[0].IsVowel() ?
                (desc.Article == Article.a ? Article.an : desc.Article) :
                desc.Article;
        return text?.
            DiagGen("{item_article}", (article != Article.NONE) ? article.ToString().ToLower() : "").
            DiagGen("{item_name}", item.ToString() ?? "<missing>").
            DiagGen("{item_nameonly}", desc.Name ?? "<missing>").
            DiagGen("{item_desc}", desc.Descriptions?.TakeRandom() ?? "... I'm not sure how to describe it").
            DiagGen("{char_name}", charName)
            ?? "<missing text>";
    }

    public static string GetWelcomeDialogueText(CharacterInstance character, ItemInstance item) {
        return ReplaceText(character.Template.WelcomeText?.TakeRandom(), item, character.Name);
    }

    public static string GetDescriptionText(CharacterInstance character, ItemInstance item) {
        // If we have detailed item descriptions, and there's a description that uses the details, try to get one of those instead
        if ((item.Item.Descriptions?.Length ?? 0) > 0) {
            var detailed = character.Template.DescribeAgain.Where(x => x.Contains("{item_desc}"));
            if (detailed.Count() > 0)
                return ReplaceText(detailed.TakeRandom(), item, character.Name);
        }
        return ReplaceText(character.Template.DescribeAgain?.TakeRandom(), item, character.Name);
    }

    public static string GetWrongItemText(CharacterInstance character, ItemInstance item) {
        return ReplaceText(character.Template.WrongItem?.TakeRandom(), item, character.Name);
    }

    public static string GetRightItemText(CharacterInstance character, ItemInstance item) {
        return ReplaceText(character.Template.RightItem?.TakeRandom(), item, character.Name);
    }

    public static string GetNoItemText(CharacterInstance character, ItemInstance item) {
        return ReplaceText(character.Template.NoItem?.TakeRandom(), item, character.Name);
    }

    internal static string GetLyingText(CharacterInstance character, ItemInstance item) {
        return ReplaceText(character.Template.Lying?.TakeRandom(), item, character.Name);
    }
}

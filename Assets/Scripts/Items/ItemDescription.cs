﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Article {
    a,
    an,
    the,
    some,
    NONE
}

[CreateAssetMenu(menuName = "Items/Description", fileName ="item_description")]
public class ItemDescription : ScriptableObject
{
    // The name that people will use when referring to this item (ex: plumbus, sunglesses, etc...)
    public string Name;
    // The multiplier to apply to this item's spawn rate
    public float WeightMultiplier = 1.0f;
    // The icon that this image will show in the containers
    public Sprite Icon;

    // The artical to use when constructing sentances
    public Article Article = Article.a;

    [Header("Randomizer Options")]
    // The available tint options for this item, or an empty list for no tint options
    public ColorTint[] TintOptions;
    // Ex: "pair", "set", 
    public string[] PrefixWords;
    // Verbal descriptions for unique items
    public string[] Descriptions;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CharacterProvider {
    private static Character[] _characters;
    private static WeightedSet<Character> _weighted;

    static CharacterProvider() {
        Resources.LoadAll<Character>("Characters");
        _characters = Resources.FindObjectsOfTypeAll<Character>();
        _weighted = new WeightedSet<Character>();
        foreach(Character character in _characters) {
            _weighted.Add(character, ((character.Names?.Length ?? 0) + 1) * character.WeightMultiplier);
        }
    }

    public static Character SelectCharacter() {
        return _weighted.Rand();
    }

    public static CharacterInstance GenerateCharacter() {
        return SelectCharacter().CreateInstance();
    }
}

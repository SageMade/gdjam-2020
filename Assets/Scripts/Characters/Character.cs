﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character", fileName = "character")]
public class Character : ScriptableObject
{
    public GameObject Prefab;
    // The multiplier to apply to this item's spawn rate
    public float WeightMultiplier = 1.0f;
    public string[] Names;

    [Header("Randomizer Options")]
    public string[] WelcomeText;
    public string[] DescribeAgain;
    public string[] WrongItem;
    public string[] RightItem;
    public string[] NoItem;
    public string[] Lying;

    [Header("SFX")]
    public AudioClip WelcomeSound; // welcome / describe again
    public AudioClip NegativeSound; // Incorrect / lying
    public AudioClip PositiveSound; // correct
    public AudioClip SadSound; // no item

    public CharacterInstance CreateInstance() {
        CharacterInstance result = new CharacterInstance();
        result.Template = this;
        result.Name = Names?.TakeRandom();
        return result;
    }
}
